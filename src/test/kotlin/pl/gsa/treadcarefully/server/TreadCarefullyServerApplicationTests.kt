package pl.gsa.treadcarefully.server

import org.junit.BeforeClass
import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.MethodSorters
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.core.ParameterizedTypeReference
import org.springframework.http.HttpEntity
import org.springframework.http.HttpMethod
import org.springframework.http.HttpStatus
import org.springframework.test.context.junit4.SpringRunner
import pl.gsa.treadcarefully.server.dto.AreaDTO
import pl.gsa.treadcarefully.server.dto.LatLngDTO
import pl.gsa.treadcarefully.server.dto.PolygonDTO
import pl.gsa.treadcarefully.server.enums.DangerLevel
import java.net.URI


@RunWith(SpringRunner::class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureRestDocs(outputDir = "build/snippets")
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
class TreadCarefullyServerApplicationTests {

    companion object {
        private var areas: ArrayList<AreaDTO> = ArrayList()

        @JvmStatic
        @BeforeClass
        fun prepareAreas() {

            val politechnika = AreaDTO(
                    null,
                    null,
                    "Warsaw University of Technology",
                    "A very dangerous place indeed.",
                    DangerLevel.LEVEL_5,
                    PolygonDTO(listOf(
                            LatLngDTO(52.222955, 21.004987),
                            LatLngDTO(52.222785, 21.008081),
                            LatLngDTO(52.220095, 21.011787),
                            LatLngDTO(52.220193, 21.005115)
                    ))
            )

            val centrum = AreaDTO(
                    null,
                    null,
                    "Warsaw centre",
                    "Might end in spending too much money on coffee",
                    DangerLevel.LEVEL_3,
                    PolygonDTO(listOf(
                            LatLngDTO(52.228048, 21.002179),
                            LatLngDTO(52.230024, 21.011151),
                            LatLngDTO(52.235103, 21.008282),
                            LatLngDTO(52.232980, 20.999001)
                    ))
            )

            val pragapln = AreaDTO(
                    null,
                    null,
                    "Praga Północ",
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer non metus eget quam sagittis elementum. Pellentesque nec purus velit. Donec sed purus ut tellus ultrices venenatis. Praesent ac venenatis nisi. Praesent in egestas arcu. Aliquam blandit in arcu non sollicitudin. Integer sit amet justo tortor. Cras a pretium odio. Morbi tempus, enim id porttitor congue, justo elit bibendum libero, tempus tempus tellus nulla commodo ante. Duis rutrum dapibus dui, et congue ex sagittis vel. Sed vulputate sem ac mattis lobortis. Cras tristique ultricies lacus, consectetur efficitur lorem elementum non.\n" +
                            "\n" +
                            "Fusce eu euismod est. Quisque at vulputate neque, pretium rhoncus augue. Praesent quam erat, semper in rhoncus vel, varius eget elit. Duis placerat volutpat mi ac molestie. Morbi fermentum facilisis quam in malesuada. Suspendisse sit amet mauris eu urna aliquam auctor sed in nunc. In sit amet egestas diam. Etiam facilisis justo in metus pharetra pharetra. Suspendisse ultrices semper dapibus. Ut sagittis, urna a imperdiet suscipit, mauris metus fringilla ex, et pulvinar dolor arcu id magna. Duis faucibus metus non suscipit ultrices.\n" +
                            "\n" +
                            "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin augue metus, iaculis vel nisl a, hendrerit elementum mi. Vivamus pellentesque arcu est, nec pharetra tortor aliquam id. Maecenas iaculis purus nisl, in viverra lacus faucibus quis. Aliquam quis tortor eu urna vehicula tincidunt fringilla sit amet eros. Donec tempus, purus ac dapibus blandit, dolor ipsum tincidunt lacus, nec blandit velit nisl nec arcu. Vivamus non metus euismod, fringilla libero vel, suscipit felis. In vitae nibh eget tellus suscipit aliquam eu eu mi. Nam convallis lobortis maximus. Donec eu aliquam felis, vel facilisis sem. Curabitur eu lacus est. Ut non sagittis metus, id pretium eros. Integer id euismod est. Vestibulum vehicula egestas nisi sit amet commodo.\n" +
                            "\n" +
                            "Curabitur fringilla erat eu tortor elementum bibendum. Nunc eu efficitur nisi. Mauris posuere non tellus ut fermentum. Nam sagittis, lectus nec lobortis venenatis, tortor justo dignissim dui, sit amet feugiat mauris dui vel erat. Aenean diam lectus, eleifend vel pellentesque sit amet, semper sed mauris. Vivamus interdum ultrices lectus, vitae aliquet metus dapibus ultrices. Vivamus in efficitur orci. Donec eu nisl eu nunc dignissim egestas. Nunc vestibulum posuere ipsum, eget pharetra libero luctus sed. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nam nec tempor nisi. Nulla tincidunt maximus purus, ut gravida metus dignissim ut. Vivamus blandit nec metus ut dictum.\n" +
                            "\n" +
                            "Nulla posuere ipsum a semper feugiat. Nam ac tincidunt lacus. Sed in tempor nisi. Integer a rhoncus risus. Proin tellus sapien, dapibus id enim nec, luctus interdum ipsum. Aliquam libero nulla, hendrerit eu enim a, aliquam dapibus nibh. Aenean gravida ac quam id interdum. Nulla consequat in nisl ut porta. Integer a pulvinar urna, sit amet varius odio. Quisque suscipit sollicitudin gravida.",
                    DangerLevel.LEVEL_2,
                    PolygonDTO(listOf(
                            LatLngDTO(52.240005, 21.039343),
                            LatLngDTO(52.242948, 21.043119),
                            LatLngDTO(52.248791, 21.044466),
                            LatLngDTO(52.258293, 21.070240),
                            LatLngDTO(52.261016, 21.064050),
                            LatLngDTO(52.268318, 21.036969),
                            LatLngDTO(52.272121, 21.026396),
                            LatLngDTO(52.293588, 21.013657),
                            LatLngDTO(52.287772, 20.997268),
                            LatLngDTO(52.281487, 21.003689),
                            LatLngDTO(52.262102, 21.013397),
                            LatLngDTO(52.250076, 21.024166)
                    ))
            )

            val zacisze = AreaDTO(
                    null,
                    null,
                    "Zacisze",
                    "Might end in meeting application developers",
                    DangerLevel.LEVEL_4,
                    PolygonDTO(listOf(
                            LatLngDTO(52.275467, 21.065538),
                            LatLngDTO(52.279589, 21.054977),
                            LatLngDTO(52.286755, 21.048423),
                            LatLngDTO(52.292008, 21.051882),
                            LatLngDTO(52.291377, 21.068390),
                            LatLngDTO(52.286946, 21.075059)
                    ))
            )

            areas.add(zacisze)
            areas.add(pragapln)
            areas.add(politechnika)
            areas.add(centrum)
        }
    }

    @Autowired
    lateinit var testRestTemplate: TestRestTemplate

    @Test
    fun contextLoads() {
    }

    @Test
    fun shouldAddNewDangerousAreas() {
        val result = testRestTemplate.exchange(URI("/api/area"), HttpMethod.POST, HttpEntity(areas),
                object : ParameterizedTypeReference<List<AreaDTO>>() {})

        assert(result.statusCode == HttpStatus.OK)
        assert(result.body.size == areas.size)

        areas = result.body as ArrayList<AreaDTO>
    }

    @Test
    fun shouldGetAllNewDangerousAreas() {
        for (area in areas) {
            val result = testRestTemplate.getForEntity(URI("/api/area/${area.id}"), AreaDTO::class.java)
            assert(result.statusCode == HttpStatus.OK)
            assert(result.body == area)
        }
    }

    @Test
    fun shouldHaveSameOrMoreAreas() {
        val result = testRestTemplate.exchange(URI("/api/area"), HttpMethod.GET, null,
                object : ParameterizedTypeReference<List<AreaDTO>>() {})

        assert(result.statusCode == HttpStatus.OK)
        assert(result.body.size >= areas.size)
    }

}
