package pl.gsa.treadcarefully.server.model

import com.vividsolutions.jts.geom.Coordinate
import com.vividsolutions.jts.geom.GeometryFactory
import com.vividsolutions.jts.geom.Polygon
import pl.gsa.treadcarefully.server.dto.AreaDTO
import pl.gsa.treadcarefully.server.dto.LatLngDTO
import pl.gsa.treadcarefully.server.dto.PolygonDTO
import pl.gsa.treadcarefully.server.enums.DangerLevel
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "area")
data class Area(@Id
                @Column(name = "id")
                @GeneratedValue(strategy = GenerationType.IDENTITY)
                val id: Int? = null,

                @Temporal(TemporalType.TIMESTAMP)
                @Column(name = "added")
                val added: Date,

                @Column(name = "name")
                val name: String,

                @Column(name = "description", columnDefinition = "text")
                val description: String,

                @Column(name = "danger_level")
                val dangerLevel: DangerLevel,

                @Column(name = "polygon")
                val polygon: Polygon,

                @Column(name = "area")
                val area: Double? = null,

                @Column(name = "bounding_circle")
                val boundingCircle: Polygon? = null) {

    fun toDTO(): AreaDTO {
        val points = ArrayList<LatLngDTO>()
        for (i in 0 until polygon.exteriorRing.numPoints) {
            val point = polygon.exteriorRing.getPointN(i)
            points.add(LatLngDTO(point.x, point.y))
        }

        return AreaDTO(
                this.id,
                this.added,
                this.name,
                this.description,
                this.dangerLevel,
                PolygonDTO(points)
        )
    }

    companion object {

        fun fromDTO(dto: AreaDTO, geometryFactory: GeometryFactory): Area {
            val shellCoordinates = ArrayList<Coordinate>()
            for (latlng in dto.polygon.points) {
                shellCoordinates.add(Coordinate(latlng.latitude, latlng.longitude))
            }

            if (shellCoordinates.first() != shellCoordinates.last()) {
                shellCoordinates.add(shellCoordinates.first())
            }

            val polygon = geometryFactory.createPolygon(shellCoordinates.toTypedArray())

            return Area(
                    dto.id,
                    dto.added ?: Date(),
                    dto.name,
                    dto.description,
                    dto.dangerLevel,
                    polygon
            )
        }

    }

}