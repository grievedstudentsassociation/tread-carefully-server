package pl.gsa.treadcarefully.server.controller

import org.springframework.web.bind.annotation.*
import pl.gsa.treadcarefully.server.dto.AreaDTO
import pl.gsa.treadcarefully.server.service.AreaService

@RestController
@RequestMapping("/api/area")
class AreaController(private val areaService: AreaService) {

    @GetMapping
    fun findAllAreas(): List<AreaDTO> {
        return areaService.findAllAreas()
    }

    @GetMapping("/{id}")
    fun getArea(@PathVariable id: Int): AreaDTO? {
        return areaService.findArea(id)
    }

    @PostMapping
    fun addAreas(@RequestBody areaDTOs: List<AreaDTO>): List<AreaDTO> {
        return areaService.addAreas(areaDTOs)
    }

}