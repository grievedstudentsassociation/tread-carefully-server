package pl.gsa.treadcarefully.server.controller

import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import pl.gsa.treadcarefully.server.dto.TestDTO

@RestController
@RequestMapping("/api/test")
class TestController {

    @PostMapping
    fun test(@RequestBody testDTO: TestDTO): TestDTO {
        return testDTO
    }

}

