package pl.gsa.treadcarefully.server.controller

import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import pl.gsa.treadcarefully.server.dto.AreaDTO
import pl.gsa.treadcarefully.server.dto.DirectionsRequestDTO
import pl.gsa.treadcarefully.server.dto.DirectionsResponseDTO
import pl.gsa.treadcarefully.server.dto.LatLngBoundsDTO
import pl.gsa.treadcarefully.server.service.AreaService
import pl.gsa.treadcarefully.server.service.DirectionsService

@RestController
@RequestMapping("/api/android")
class AndroidController(private val areaService: AreaService,
                        private val directionsService: DirectionsService) {

    @PostMapping("/area")
    fun findAreasInBounds(@RequestBody bounds: LatLngBoundsDTO): List<AreaDTO> {
        return areaService.findAreasInBounds(bounds)
    }

    @PostMapping("/directions")
    fun findDirections(@RequestBody directionsRequest: DirectionsRequestDTO): DirectionsResponseDTO {
        return directionsService.findDirections(directionsRequest)
    }

}