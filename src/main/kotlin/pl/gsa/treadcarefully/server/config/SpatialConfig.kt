package pl.gsa.treadcarefully.server.config

import com.vividsolutions.jts.geom.GeometryFactory
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class SpatialConfig {

    @Bean
    fun geometryFactory(): GeometryFactory = GeometryFactory()

}