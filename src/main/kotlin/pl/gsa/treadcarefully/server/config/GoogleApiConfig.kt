package pl.gsa.treadcarefully.server.config

import com.google.maps.GeoApiContext
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import pl.gsa.treadcarefully.server.properties.GoogleProperties

@Configuration
class GoogleApiConfig(private val googleProperties: GoogleProperties) {

    @Bean
    fun geoApiContext(): GeoApiContext = GeoApiContext.Builder()
            .apiKey(googleProperties.apiKey)
            .build()

}