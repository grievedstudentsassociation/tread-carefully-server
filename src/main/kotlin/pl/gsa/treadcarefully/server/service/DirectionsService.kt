package pl.gsa.treadcarefully.server.service

import com.google.maps.DirectionsApi
import com.google.maps.GeoApiContext
import com.google.maps.model.LatLng
import com.google.maps.model.TravelMode
import com.vividsolutions.jts.geom.*
import com.vividsolutions.jts.geom.util.LineStringExtracter
import com.vividsolutions.jts.geom.util.PointExtracter
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import pl.gsa.treadcarefully.server.dto.DirectionsRequestDTO
import pl.gsa.treadcarefully.server.dto.DirectionsResponseDTO
import pl.gsa.treadcarefully.server.dto.LatLngDTO
import pl.gsa.treadcarefully.server.model.Area
import pl.gsa.treadcarefully.server.repository.AreaRepository
import kotlin.math.PI
import kotlin.math.cos
import kotlin.math.sin

@Service
class DirectionsService(private val areaRepository: AreaRepository,
                        private val geoApiContext: GeoApiContext,
                        private val geometryFactory: GeometryFactory) {

    private val log = LoggerFactory.getLogger(this.javaClass)

    fun findDirections(directionsRequest: DirectionsRequestDTO): DirectionsResponseDTO {
        log.debug("Finding directions from: ${directionsRequest.origin} to ${directionsRequest.destination}")

        // Fetch initial line
        val initialLineString = fetchRouteLineString(directionsRequest, emptyArray())
        if (initialLineString == null) {
            log.debug("Couldn't find directions from: ${directionsRequest.origin} to ${directionsRequest.destination}")
            return DirectionsResponseDTO(emptyList())
        }

        log.debug("Found ${initialLineString.numPoints}-point path " +
                "from: ${directionsRequest.origin} to ${directionsRequest.destination}")

        // Find areas crossed by the line
        val crossedAreas = areaRepository.findAreasOnTheLine(initialLineString)

        // Don't try to avoid starting and final areas if they're dangerous
        crossedAreas.filter {
            it.polygon.contains(initialLineString.startPoint) || it.polygon.contains(initialLineString.endPoint)
        }.forEach { crossedAreas.remove(it) }

        var safeLineString: LineString? = null

        if (crossedAreas.isNotEmpty()) {
            // Find points to omit and use them to fetch safe line
            val omittingPoints = extractOmittingPoints(initialLineString, crossedAreas)
            log.debug("Found dangerous area omitting points: $omittingPoints")

            safeLineString = fetchRouteLineString(directionsRequest, omittingPoints.toTypedArray())
        }

        // Return safe line or initial one in case safe one couldn't be obtained
        return buildDirectionsResponse(safeLineString ?: initialLineString)
    }

    private fun buildDirectionsResponse(lineString: LineString): DirectionsResponseDTO {
        val points = ArrayList<LatLngDTO>()

        for (i in 0 until lineString.numPoints) {
            points.add(LatLngDTO(lineString.getPointN(i).x, lineString.getPointN(i).y))
        }

        return DirectionsResponseDTO(points)
    }

    private fun fetchRouteLineString(directionsRequest: DirectionsRequestDTO, omittingPoints: Array<LatLng>): LineString? {
        val initialRequest = DirectionsApi.newRequest(geoApiContext)
                .mode(TravelMode.WALKING)
                .origin(LatLng(directionsRequest.origin.latitude, directionsRequest.origin.longitude))
                .destination(LatLng(directionsRequest.destination.latitude, directionsRequest.destination.longitude))
                .waypoints(*omittingPoints)
                .optimizeWaypoints(true)
                .await()

        if (initialRequest.routes.isEmpty()) return null

        val coordinates = ArrayList<Coordinate>()

        initialRequest.routes[0].overviewPolyline.decodePath()
                .mapTo(coordinates) { Coordinate(it.lat, it.lng) }

        return geometryFactory.createLineString(coordinates.toTypedArray())
    }

    @Suppress("UNCHECKED_CAST")
    private fun extractOmittingPoints(initialLineString: LineString, crossedAreas: List<Area>): List<LatLng> {
        // Find route segments that intersect circles
        // Maps bounding circles into entrance/exit point pairs
        val polygonIntersectionMap = HashMap<Area, Pair<Point, Point>>()

        for (i in 0 until initialLineString.numPoints - 1) {

            val lineSegment = LineSegment(initialLineString.getCoordinateN(i), initialLineString.getCoordinateN(i + 1))

            val area = crossedAreas.singleOrNull { it.polygon.exteriorRing.intersects(lineSegment.toGeometry(geometryFactory)) }

            if (area != null) {

                val point = PointExtracter.getPoints(area.polygon.exteriorRing.intersection(lineSegment.toGeometry(geometryFactory)))[0] as Point

                val pair = polygonIntersectionMap.getOrPut(area, { Pair(point, point) })

                if (point != pair.second) {
                    polygonIntersectionMap.put(area, Pair(pair.first, point))
                }
            }
        }

        // Find two points of the triangle base
        val circleIntersectionMap = HashMap<Area, LineSegment>()

        for (area in polygonIntersectionMap.keys) {
            val entranceExitPair = polygonIntersectionMap.getValue(area)

            val circleRadius = area.boundingCircle!!.centroid.distance(area.boundingCircle.exteriorRing.startPoint)

            val entranceExitSegment = LineSegment(entranceExitPair.first.coordinate, entranceExitPair.second.coordinate)

            val secantLineString = LineStringExtracter.getLines(
                    area.boundingCircle.intersection(
                            enlongLineSegment(entranceExitSegment, circleRadius)
                                    .toGeometry(geometryFactory))
            )[0] as LineString

            val secantLineSegment = LineSegment(secantLineString.startPoint.coordinate, secantLineString.endPoint.coordinate)

            circleIntersectionMap.put(area, secantLineSegment)
        }

        val omittingPoints = ArrayList<LatLng>()

        // Take half of the secant and rotate it by 90 degrees in both directions. If found intersection point, take it
        for (area in circleIntersectionMap.keys) {
            val secantLineSegment = circleIntersectionMap.getValue(area)

            val halfSecantLineSegment = LineSegment(secantLineSegment.midPoint(), secantLineSegment.p1)

            val firstRotated = rotateLineSegment(halfSecantLineSegment, PI / 2)
            val secondRotated = rotateLineSegment(halfSecantLineSegment, -PI / 2)

            val firstIntersectionPoints = PointExtracter.getPoints(
                    area.boundingCircle!!.exteriorRing.intersection(firstRotated.toGeometry(geometryFactory))
            ) as List<Point>
            val secondIntersectionPoints = PointExtracter.getPoints(
                    area.boundingCircle.exteriorRing.intersection(secondRotated.toGeometry(geometryFactory))
            ) as List<Point>

            if (firstIntersectionPoints.size == 1) {
                omittingPoints.add(LatLng(firstIntersectionPoints[0].x, firstIntersectionPoints[0].y))
            } else if (secondIntersectionPoints.size == 1) {
                omittingPoints.add(LatLng(secondIntersectionPoints[0].x, secondIntersectionPoints[0].y))
            }
        }

        return omittingPoints
    }

    private fun enlongLineSegment(lineSegment: LineSegment, extraLength: Double): LineSegment {
        val rightAngle = lineSegment.angle()
        val leftAngle = PI + rightAngle

        var right = LineSegment(lineSegment.p1, Coordinate(lineSegment.p1).apply { this.x += extraLength })
        var left = LineSegment(lineSegment.p0, Coordinate(lineSegment.p0).apply { this.x += extraLength })

        right = rotateLineSegment(right, rightAngle)
        left = rotateLineSegment(left, leftAngle)

        return LineSegment(left.p1, right.p1)
    }

    private fun rotateLineSegment(lineSegment: LineSegment, angle: Double): LineSegment = LineSegment(
            lineSegment.p0.x,
            lineSegment.p0.y,
            (lineSegment.p1.x - lineSegment.p0.x) * cos(angle) - (lineSegment.p1.y - lineSegment.p0.y) * sin(angle) + lineSegment.p0.x,
            (lineSegment.p1.x - lineSegment.p0.x) * sin(angle) + (lineSegment.p1.y - lineSegment.p0.y) * cos(angle) + lineSegment.p0.y
    )

}