package pl.gsa.treadcarefully.server.service

import com.vividsolutions.jts.geom.Coordinate
import com.vividsolutions.jts.geom.GeometryFactory
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import pl.gsa.treadcarefully.server.dto.AreaDTO
import pl.gsa.treadcarefully.server.dto.LatLngBoundsDTO
import pl.gsa.treadcarefully.server.model.Area
import pl.gsa.treadcarefully.server.repository.AreaRepository
import javax.transaction.Transactional

@Service
class AreaService(private val areaRepository: AreaRepository,
                  private val geometryFactory: GeometryFactory) {

    private val log = LoggerFactory.getLogger(this.javaClass)

    fun findAllAreas(): List<AreaDTO> {
        log.info("Getting all areas")

        val areaDTOs = ArrayList<AreaDTO>()
        areaRepository.findAll().mapTo(areaDTOs) { it.toDTO() }

        log.info("Found areas: $areaDTOs")
        return areaDTOs
    }

    fun findArea(id: Int): AreaDTO? {
        log.info("Searching for area with id: $id")
        var areaDTO: AreaDTO? = null

        areaRepository.findById(id).ifPresent {
            areaDTO = it.toDTO()
        }

        log.info("Found area with id $id: $areaDTO")
        return areaDTO
    }

    @Transactional
    fun findAreasInBounds(bounds: LatLngBoundsDTO): List<AreaDTO> {
        log.debug("Searching for areas within bounds: $bounds")

        val boundsRectangle = geometryFactory.createPolygon(arrayOf(
                Coordinate(bounds.northEast.latitude, bounds.northEast.longitude),
                Coordinate(bounds.northEast.latitude, bounds.southWest.longitude),
                Coordinate(bounds.southWest.latitude, bounds.southWest.longitude),
                Coordinate(bounds.southWest.latitude, bounds.northEast.longitude),
                Coordinate(bounds.northEast.latitude, bounds.northEast.longitude)
        ))

        val areaDTOs = ArrayList<AreaDTO>()
        areaRepository.findAreasInBounds(boundsRectangle, boundsRectangle.area).mapTo(areaDTOs) { it.toDTO() }

        log.debug("Found areas: $areaDTOs")
        return areaDTOs
    }

    @Transactional
    fun addAreas(areaDTOs: List<AreaDTO>): List<AreaDTO> {
        log.info("Saving new areas: $areaDTOs")

        val savedAreas = ArrayList<AreaDTO>()

        for (areaDTO in areaDTOs) {
            val savedArea = areaRepository.save(Area.fromDTO(areaDTO, geometryFactory))
            savedAreas.add(savedArea.toDTO())
        }

        return savedAreas
    }

}

