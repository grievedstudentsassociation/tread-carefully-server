package pl.gsa.treadcarefully.server.dto

data class LatLngDTO(var latitude: Double,
                     var longitude: Double)