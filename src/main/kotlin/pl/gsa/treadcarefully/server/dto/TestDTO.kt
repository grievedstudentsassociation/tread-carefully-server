package pl.gsa.treadcarefully.server.dto

data class TestDTO(val text: String? = null, val id: Int? = null)
