package pl.gsa.treadcarefully.server.dto

data class DirectionsResponseDTO(var points: List<LatLngDTO>)