package pl.gsa.treadcarefully.server.dto

data class DirectionsRequestDTO(var origin: LatLngDTO,
                                var destination: LatLngDTO)