package pl.gsa.treadcarefully.server.dto

data class PolygonDTO(var points: List<LatLngDTO>)