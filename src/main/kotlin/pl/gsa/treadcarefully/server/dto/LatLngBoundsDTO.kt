package pl.gsa.treadcarefully.server.dto

data class LatLngBoundsDTO(var northEast: LatLngDTO,
                           var southWest: LatLngDTO)