package pl.gsa.treadcarefully.server.repository

import com.vividsolutions.jts.geom.LineString
import com.vividsolutions.jts.geom.Polygon
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.data.repository.query.Param
import pl.gsa.treadcarefully.server.model.Area

interface AreaRepository : CrudRepository<Area, Int> {

    // language=JPAQL
    @Query("SELECT a FROM Area a " +
            "WHERE intersects(a.polygon, :boundsRectangle) = TRUE " +
            "AND a.area / :boundsRectangleArea >= 0.0001")
    fun findAreasInBounds(@Param("boundsRectangle") boundsRectangle: Polygon,
                          @Param("boundsRectangleArea") boundsRectangleArea: Double): List<Area>

    // language=JPAQL
    @Query("SELECT a FROM Area a " +
            "WHERE intersects(a.polygon, :line) = TRUE")
    fun findAreasOnTheLine(@Param("line") line: LineString): ArrayList<Area>

}