package pl.gsa.treadcarefully.server

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class TreadCarefullyServerApplication

fun main(args: Array<String>) {
    runApplication<TreadCarefullyServerApplication>(*args)
}
