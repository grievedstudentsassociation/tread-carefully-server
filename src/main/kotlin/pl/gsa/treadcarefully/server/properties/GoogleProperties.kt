package pl.gsa.treadcarefully.server.properties

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.stereotype.Component

@Component
@ConfigurationProperties("tc.google")
class GoogleProperties {

    lateinit var apiKey: String

}