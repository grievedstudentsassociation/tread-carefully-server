package pl.gsa.treadcarefully.server.enums

enum class DangerLevel {
    LEVEL_1, LEVEL_2, LEVEL_3, LEVEL_4, LEVEL_5
}