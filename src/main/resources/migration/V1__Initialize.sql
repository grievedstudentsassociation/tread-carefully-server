CREATE TABLE tc_data.area (
  id           SERIAL PRIMARY KEY,
  added        TIMESTAMP,
  danger_level INTEGER,
  description  TEXT,
  name         VARCHAR(255),
  area         DOUBLE PRECISION
);

SELECT AddGeometryColumn('tc_data', 'area', 'polygon', 0, 'POLYGON', 2);
SELECT AddGeometryColumn('tc_data', 'area', 'bounding_circle', 0, 'POLYGON', 2);

CREATE INDEX area_area_idx
  ON tc_data.area (area);

CREATE FUNCTION calculate_polygon_auxiliary_data()
  RETURNS TRIGGER AS
$$
BEGIN
  NEW.area = ST_Area(NEW.polygon);
  NEW.bounding_circle = ST_MinimumBoundingCircle(NEW.polygon);
  RETURN NEW;
END;
$$
LANGUAGE plpgsql;

CREATE TRIGGER calculate_polygon_auxiliary_data_trg
  BEFORE INSERT OR UPDATE OF polygon, area, bounding_circle
  ON tc_data.area
  FOR EACH ROW
EXECUTE PROCEDURE calculate_polygon_auxiliary_data();