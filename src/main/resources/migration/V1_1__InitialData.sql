INSERT INTO tc_data.area (added, danger_level, name, description, polygon)
VALUES
  (current_timestamp, 4,
   'Warsaw University of Technology',
   'A very dangerous place indeed',
   ST_GeometryFromText('POLYGON((' ||
                       '52.222955 21.004987, ' ||
                       '52.222785 21.008081, ' ||
                       '52.220095 21.011787, ' ||
                       '52.220193 21.005115, ' ||
                       '52.222955 21.004987))', 0)),
  (current_timestamp, 2,
   'Warsaw centre',
   'Might end in spending too much money on coffee',
   ST_GeometryFromText('POLYGON((' ||
                       '52.225419 20.996644, ' ||
                       '52.229818 21.016756, ' ||
                       '52.232028 21.015876, ' ||
                       '52.232428 21.014863, ' ||
                       '52.233020 21.012694, ' ||
                       '52.235585 21.010821, ' ||
                       '52.235097 21.008301, ' ||
                       '52.233930 21.002569, ' ||
                       '52.233306 20.999651, ' ||
                       '52.232615 20.999073, ' ||
                       '52.232608 20.998044, ' ||
                       '52.232618 20.996969, ' ||
                       '52.231814 20.992762, ' ||
                       '52.230190 20.985234, ' ||
                       '52.229727 20.985470, ' ||
                       '52.225229 20.989217, ' ||
                       '52.224990 20.989649, ' ||
                       '52.224549 20.989767, ' ||
                       '52.223978 20.990094, ' ||
                       '52.225419 20.996644))', 0)),
  (current_timestamp, 3,
   'Zacisze',
   'Might end in meeting application developers',
   ST_GeometryFromText('POLYGON((' ||
                       '52.275467 21.065538, ' ||
                       '52.279589 21.054977, ' ||
                       '52.286755 21.048423, ' ||
                       '52.292008 21.051882, ' ||
                       '52.291377 21.068390, ' ||
                       '52.286946 21.075059, ' ||
                       '52.275467 21.065538))', 0)),
  (current_timestamp, 1,
   'Praga Północ',
   'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris eros risus, facilisis vitae eleifend convallis, consectetur ac mauris. Pellentesque ex libero, consectetur sed porttitor sit amet, finibus in leo. In a mauris leo. Integer porttitor, nisi quis tincidunt blandit, velit purus tempus urna, ut gravida sapien erat sed sapien. Nam porttitor magna diam, vitae ornare felis bibendum nec. Cras rhoncus magna est, nec accumsan tellus condimentum eu. Duis viverra, arcu nec aliquet suscipit, dolor arcu ullamcorper velit, at imperdiet massa libero nec justo. Cras vulputate lorem eget leo volutpat pellentesque. Fusce nulla purus, vestibulum quis arcu et, mattis dictum leo. Proin id dignissim diam, in gravida dolor. Donec consectetur convallis posuere. Quisque dignissim euismod velit nec viverra. Aliquam eu ipsum eget nisi rhoncus placerat a in quam. ' ||
   'Donec pellentesque euismod dolor a ultrices. Morbi magna enim, congue ac ipsum vitae, facilisis mattis leo. Morbi malesuada tristique odio sit amet convallis. Cras sed hendrerit ligula. Curabitur nec tortor neque. Aenean a aliquet leo. Mauris non nulla mattis, dictum eros ullamcorper, lacinia enim. Etiam in mollis odio, sit amet dapibus ligula. Morbi tincidunt viverra tellus, et fermentum velit consectetur non. In nisl sem, tincidunt eu nibh et, sagittis malesuada odio. Fusce ullamcorper tincidunt felis vitae commodo. Ut ut risus in justo malesuada hendrerit id vitae sem. Etiam pretium odio justo, venenatis accumsan neque bibendum non. Nulla facilisi. ' ||
   'Sed pulvinar quam eget leo euismod sodales. Curabitur feugiat tincidunt ante ut malesuada. Vivamus vel velit turpis. Etiam leo sem, facilisis id justo at, sodales blandit sem. Donec vitae lobortis ligula. Nunc vel odio eu quam sodales condimentum in egestas libero. Fusce in dui tortor. Ut ultricies eget purus ac interdum. In hac habitasse platea dictumst. Praesent fringilla dui nisi. ' ||
   'Pellentesque porttitor tristique tortor maximus ullamcorper. Nullam rutrum libero a tellus ultricies, id blandit magna tristique. Pellentesque faucibus ex ut est hendrerit interdum. Suspendisse ut lectus sit amet mauris tempor hendrerit. Curabitur eget nibh non metus fringilla tincidunt porttitor non nisl. Nunc justo metus, commodo at diam vel, pharetra faucibus ante. Aliquam sagittis, urna ut pharetra mollis, massa quam convallis felis, sit amet convallis ante velit non dolor. Etiam suscipit egestas enim eget laoreet. Curabitur tincidunt, odio nec interdum cursus, urna tellus commodo mi, nec auctor mauris nunc sed nulla. ' ||
   'Nullam a posuere lorem. Pellentesque quis lectus sit amet nulla sollicitudin consequat. Curabitur fringilla erat non tortor elementum scelerisque. Aenean sed gravida odio. Quisque iaculis libero a elit aliquam tristique. Nullam nunc nibh, commodo et ante ut, cursus sagittis sem. Vestibulum maximus lacinia vehicula. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Integer ornare eros eu blandit condimentum. Praesent pharetra leo lacus, eget dictum leo porta at. Aliquam varius augue vel luctus cursus. Praesent non lacinia odio. Morbi lorem diam, rutrum eu aliquet sed, egestas sed eros. In sed hendrerit neque. Donec eu arcu ornare, malesuada libero ultricies, accumsan ex.',
   ST_GeometryFromText('POLYGON((' ||
                       '52.293342 21.015301, ' ||
                       '52.291259 21.017826, ' ||
                       '52.289258 21.019425, ' ||
                       '52.285364 21.022230, ' ||
                       '52.276789 21.028376, ' ||
                       '52.268790 21.036713, ' ||
                       '52.267369 21.039624, ' ||
                       '52.263438 21.054454, ' ||
                       '52.262486 21.054629, ' ||
                       '52.255459 21.048963, ' ||
                       '52.254420 21.043728, ' ||
                       '52.253523 21.040734, ' ||
                       '52.252369 21.037724, ' ||
                       '52.248275 21.028719, ' ||
                       '52.248746 21.027212, ' ||
                       '52.250461 21.025121, ' ||
                       '52.250694 21.024837, ' ||
                       '52.253674 21.022373, ' ||
                       '52.261465 21.015371, ' ||
                       '52.263145 21.014237, ' ||
                       '52.278433 21.008693, ' ||
                       '52.282515 21.006320, ' ||
                       '52.285622 21.003719, ' ||
                       '52.288627 21.000389, ' ||
                       '52.289712 21.004485, ' ||
                       '52.290960 21.005786, ' ||
                       '52.293342 21.015301))', 0)),
  (current_timestamp, 3,
   'Plac Naturowicza',
   'Might end in meeting drunk students',
   ST_GeometryFromText('POLYGON((' ||
                       '52.216155 20.982139, ' ||
                       '52.216564 20.987874, ' ||
                       '52.216910 20.988170, ' ||
                       '52.219071 20.989126, ' ||
                       '52.220565 20.989364, ' ||
                       '52.221029 20.984974, ' ||
                       '52.221640 20.980790, ' ||
                       '52.217235 20.981528, ' ||
                       '52.216552 20.980944, ' ||
                       '52.216155 20.982139))', 0));
