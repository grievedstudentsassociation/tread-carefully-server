FROM openjdk:8u151-jre-alpine3.7 as base
EXPOSE 11643
STOPSIGNAL SIGINT

FROM openjdk:8u151-jdk-alpine3.7 as build
WORKDIR /app

COPY . .
RUN ./build_prod.sh

FROM base as final
WORKDIR /root

RUN apk add --no-cache netcat-openbsd
COPY --from=build /app/build/libs/*.jar ./tc-server.jar

COPY docker/prod/docker-entrypoint.sh .
RUN chmod +x *.sh

ENTRYPOINT ["./docker-entrypoint.sh"]