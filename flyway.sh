#!/usr/bin/env bash
TYPE="$1"
COMMAND="$2"

if [ -z "$TYPE" ] || [ -z "$COMMAND" ]; then
    echo "Executes Flyway's COMMAND on the database of type TYPE"
    echo ""
    echo "USAGE: ./flyway.sh TYPE COMMAND"
    echo ""
    echo "TYPE - prod, dev"
    echo "COMMAND - flywayMigrate, flywayClean, flywayInfo, flywayValidate, flywayBaseline, flywayRepair"
else
    ./gradlew ${COMMAND} -Dflyway.configFiles=config/flyway-${TYPE}.properties
fi