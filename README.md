# Tread Carefully - Server  #
    
### Prerequisities for deployment ###

* Linux server
* Docker Community Edition Platform (v17.12 or newer) and Docker Compose (v1.18 or newer)

### Prerequisities for development ###

* Java Development Kit 1.8 or newer
* [Gradle](https://gradle.org/) (Gradle wrapper, included in the project, can be used as well)

### Preparing development environment (using IntelliJ IDEA) ###

* Import project to IDEA
* Add Spring Boot configuration:
    * Configure it to use `Application` class as a main one
* Configure project structure:
    * Add Spring facet (in case it's not automatically detected)
* Enable annotation processing for the project
* Start Spring Boot configuration

### Preparing production environment ###

* Put the code repository on the server 
(eg. with ssh or by cloning the repository directly on the server)
* Start Docker daemon
* Execute command `docker-compose -f docker/prod/docker-compose.prod.yml up --build -d`