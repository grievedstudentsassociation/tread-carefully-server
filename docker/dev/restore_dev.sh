#!/usr/bin/env bash

BACKUP_FILENAME=$1

echo Restoring TC development database from file ${BACKUP_FILENAME}...
docker exec -i tc-database-dev pg_restore -U tc_dev -d tc_dev --format=custom --clean --create < ${BACKUP_FILENAME}
echo Restore finished
