#!/usr/bin/env bash

BACKUP_FILENAME="tc_dev_data_$(date '+%Y-%m-%d-%H-%M-%S').pgbackup"

echo Backing up TC development database...
docker exec -it tc-database-dev pg_dump -U tc_dev tc_dev --format=custom > ${BACKUP_FILENAME}
echo TC development database saved to file ${BACKUP_FILENAME}
