#!/usr/bin/env bash

BACKUP_FILENAME=$1

echo Stopping TC application server...
docker stop tc-server &>/dev/null

echo Restoring TC database from file ${BACKUP_FILENAME}...
docker exec -i tc-database pg_restore -U tc -d tc --format=custom --clean --create < ${BACKUP_FILENAME}
echo Restore finished