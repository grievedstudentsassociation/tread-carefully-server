#!/usr/bin/env sh

echo '=> Waiting for the database server to start'

while ! nc -z tc-database 5432 > /dev/null 2>&1; do
    sleep 1
done

echo '=> Database server is up - starting Tread Carefully server'

exec java -jar tc-server.jar