#!/usr/bin/env bash

BACKUP_FILENAME="tc_data_$(date '+%Y-%m-%d-%H-%M-%S').pgbackup"

echo Backing up TC database...
docker exec -it tc-database pg_dump -U tc tc --format=custom > ${BACKUP_FILENAME}
echo TC database saved to file ${BACKUP_FILENAME}
